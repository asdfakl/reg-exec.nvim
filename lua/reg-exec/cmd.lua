local Job = require'plenary.job'

RegExecCommand = {}
RegExecCommand.__index = RegExecCommand

local function is_table(x)
  assert(type(x) == 'table', 'value not of type table')
  return x
end

local function is_non_empty_string(x)
  assert(type(x) == 'string', 'value not of type string')
  x = vim.trim(x)
  assert(x ~= '', 'value is an empty string')

  return x
end

local function one_of(choices)
  local m = {}

  for _, opt in ipairs(choices) do
    m[opt] = true
  end

  return function(x)
    assert(m[x], 'value is not a valid option, valid options are: ' .. table.concat(choices, ', '))
    return x
  end
end

local function every(validator)
  return function(x)
    x = is_table(x)

    for i, v in ipairs(x) do
      x[i] = validator(v)
    end

    return x
  end
end

local function every_key_and_value(key_validator, value_validator)
  return function(x)
    x = is_table(x)

    for k, v in pairs(x) do
      k, v = key_validator(k), value_validator(v)

      x[k] = v
    end

    return x
  end
end

local function nil_or(x, validator)
  if x == nil then
    return nil
  end
  return validator(x)
end

function RegExecCommand:new(key, opts)
  local function with_ctx(ctx, f)
    local caught_err
    local ok = xpcall(f, function(err)
      caught_err = err
    end)

    if not ok then
      error("'" .. tostring(key) .. "': " .. ctx .. ': ' .. caught_err)
    end
  end

  with_ctx('key', function() key = is_non_empty_string(key) end)
  with_ctx('opts', function() opts = is_table(opts) end)

  local cmd = {key = key}

  with_ctx('opts.command', function() cmd.command = is_non_empty_string(opts.command) end)
  with_ctx('opts.args', function() cmd.args = nil_or(opts.args, every(is_non_empty_string)) end)
  with_ctx('opts.cwd', function() cmd.cwd = nil_or(opts.cwd, is_non_empty_string) end)
  with_ctx('opts.description', function() cmd.description = nil_or(opts.description, is_non_empty_string) end)
  cmd.allow_nonzero_exit = opts.allow_nonzero_exit and true or false
  cmd.stdin = opts.stdin and true or false
  with_ctx('opts.hide_stderr', function() cmd.hide_stderr = nil_or(opts.hide_stderr, one_of({'on_success', 'always'})) end)
  with_ctx('opts.window_pos', function() cmd.window_pos = nil_or(opts.window_pos, one_of({'left', 'right', 'top', 'bottom'})) or 'bottom' end)
  with_ctx('opts.mappings', function() cmd.mappings = nil_or(opts.mappings, every_key_and_value(is_non_empty_string, one_of({'close'}))) end)
  with_ctx('opts.env', function()
    cmd.env = nil_or(opts.env, function(x)
      if x == true then
        return x
      end

      return is_table(x)
    end)
  end)

  setmetatable(cmd, RegExecCommand)
  return cmd
end

function RegExecCommand:_get_description()
  return self.description or self.key
end

function RegExecCommand:_create_win_and_buf()
  local win, buf, win_cmd

  if self.window_pos == 'left' then
    win_cmd = 'topleft vnew'
  elseif self.window_pos == 'right' then
    win_cmd = 'botright vnew'
  elseif self.window_pos == 'top' then
    win_cmd = 'topleft new'
  elseif self.window_pos == 'bottom' then
    win_cmd = 'botright new'
  end

  vim.api.nvim_command(win_cmd)
  win = vim.api.nvim_get_current_win()
  buf = vim.api.nvim_get_current_buf()

  vim.api.nvim_buf_set_name(0, "reg-exec '" .. self.key .. "' result #" .. buf)

  vim.api.nvim_buf_set_option(0, 'buftype', 'nofile')
  vim.api.nvim_buf_set_option(0, 'bufhidden', 'wipe')
  vim.api.nvim_buf_set_option(0, 'swapfile', false)
  vim.api.nvim_buf_set_option(0, 'modifiable', false)
  vim.api.nvim_buf_set_option(0, 'buflisted', false)

  vim.api.nvim_command('setlocal nowrap')
  vim.api.nvim_command('setlocal cursorline')

  for sequence, action in pairs(self.mappings or {}) do
    if action == 'close' then
      vim.api.nvim_buf_set_keymap(
        0, 'n', sequence,
        "<cmd>lua require'reg-exec'.close_window_by_key('" .. self.key .. "')<cr>",
        { noremap = true, silent = true }
      )
    end
  end

  return win, buf
end

function RegExecCommand:exec_register_async(register)
  register = register or '"'

  local input = vim.fn.getreg(register)
  assert(input, "register '" .. register .. "' is empty")

  self:exec_async(input)
end

function RegExecCommand:exec_range_async(from, to)
  from = from - 1

  local lines = vim.api.nvim_buf_get_lines(0, from, to, true)
  assert(#lines > 0, 'empty range selection')

  self:exec_async(table.concat(lines, '\n'))
end

function RegExecCommand:exec_async(input)
  assert(not self.running, 'command "' .. self.key .. '" is already running')

  local args = {}

  for i, arg in ipairs(self.args or {}) do
    args[i] = arg:gsub('{}', input)
  end

  local env = {}

  if type(self.env) == 'table' then
    -- explicitly defined in command
    for k, v in pairs(self.env) do
      env[k] = v
    end
  elseif self.env then
    --copy current env
    for k, v in pairs(vim.fn.environ()) do
      env[k] = v
    end
  end

  local job, return_val

  local handle_result = function()
    self.running = false

    if self.win and vim.api.nvim_win_is_valid(self.win) then
      vim.api.nvim_set_current_win(self.win)
    else
      self.win, self.buf = self:_create_win_and_buf()
    end

    vim.api.nvim_buf_set_option(self.buf, 'modifiable', true)

    local failed = not self.allow_nonzero_exit and return_val ~= 0

    local stdout = job:result()
    vim.api.nvim_buf_set_lines(self.buf, 0, -1, true, stdout)

    if (not failed and not self.hide_stderr) or (failed and self.hide_stderr ~= 'always') then
      local stderr = job:stderr_result()

      vim.api.nvim_buf_set_lines(self.buf, #stdout, -1, true, stderr)

      for i, _ in ipairs(stderr) do
        vim.api.nvim_buf_add_highlight(self.buf, -1, 'ErrorMsg', i + #stdout - 1, 0, -1)
      end
    end

    vim.api.nvim_buf_set_option(self.buf, 'modifiable', false)

    if failed then
      error('Command ' .. self.key .. ' returned with exit code ' .. return_val)
    end
  end

  self.running = true

  Job:new({
    command = self.command,
    args = args,
    cwd = self.cwd or vim.fn.getcwd(),
    env = env,
    writer = self.stdin and input or nil,
    on_start = function()
      print("RegExec command '" .. self.key .. "' started")
    end,
    on_exit = function(j, exit_code)
      print("RegExec command '" .. self.key .. "' exited")
      job, return_val = j, exit_code
      vim.defer_fn(handle_result, 10)
    end,
  }):start()
end
