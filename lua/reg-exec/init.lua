local M = {}

local ctx = {
  win = nil, buf = nil,
  setup_called = false,
  registered_commands = {},
}

local function create_win()
  vim.api.nvim_command('botright new')
  ctx.win = vim.api.nvim_get_current_win()
  ctx.buf = vim.api.nvim_get_current_buf()

  vim.api.nvim_buf_set_name(0, 'reg-exec result #' .. ctx.buf)

  vim.api.nvim_buf_set_option(0, 'buftype', 'nofile')
  vim.api.nvim_buf_set_option(0, 'swapfile', false)
  vim.api.nvim_buf_set_option(0, 'modifiable', false)
  vim.api.nvim_buf_set_option(0, 'filetype', 'result')
  vim.api.nvim_buf_set_option(0, 'bufhidden', 'wipe')

  vim.api.nvim_command('setlocal nowrap')
  vim.api.nvim_command('setlocal cursorline')
end

local function exec_and_display(cmd)
  cmd = tostring(cmd)

  assert(cmd ~= '', 'empty command')

  local result = vim.fn.systemlist(cmd)

  vim.api.nvim_buf_set_option(ctx.buf, 'modifiable', true)

  vim.api.nvim_buf_set_lines(ctx.buf, 0, -1, false, result)

  vim.api.nvim_buf_set_option(ctx.buf, 'modifiable', false)
end

local function select_command_by_telescope(cb)
  local pickers = require 'telescope.pickers'
  local finders = require 'telescope.finders'
  local config = require 'telescope.config'.values
  local actions = require 'telescope.actions'
  local action_state = require 'telescope.actions.state'

  local entries = {}
  for key, cmd in pairs(ctx.registered_commands) do
    entries[#entries+1] = {
      cmd:_get_description(),
      key,
    }
  end

  if not (#entries > 0) then
    error('no commands registered (tip: first call setup() )')
  end

  pickers.new(ctx.telescope_opts, {
    prompt_title = 'Reg Exec',
    finder = finders.new_table{
      results = entries,
      entry_maker = function(entry)
        return {
          value = entry,
          display = entry[1],
          ordinal = entry[1],
        }
      end,
    },
    sorter = config.generic_sorter(ctx.telescope_opts),
    attach_mappings = function(prompt_bufnr, _)
      actions.select_default:replace(function()
        actions.close(prompt_bufnr)
        local selection = action_state.get_selected_entry()
        local key = selection.value[2]
        local cmd = ctx.registered_commands[key]
        assert(cmd, "command not registered for key '" .. key .. "'")
        cb(cmd)
      end)
      return true
    end,
  }):find()
end

M.exec_register_sync = function(register)
  local input = vim.fn.getreg(register or '"')

  assert(input, "empty selection")

  if ctx.win and vim.api.nvim_win_is_valid(ctx.win) then
    vim.api.nvim_set_current_win(ctx.win)
  else
    create_win()
  end

  exec_and_display(input)
end

M.close_window_by_key = function(key)
  local cmd = ctx.registered_commands[key]
  assert(cmd, "command not found by key '" .. key .. "'")

  if cmd.win and vim.api.nvim_win_is_valid(cmd.win) then
    vim.api.nvim_win_close(cmd.win, true)
  end
end

M.exec_register_async_by_telescope = function(register)
  select_command_by_telescope(function(cmd)
    cmd:exec_register_async(register)
  end)
end

M.exec_range_async_by_telescope = function(from, to)
  select_command_by_telescope(function(cmd)
    cmd:exec_range_async(from, to)
  end)
end

M.exec_register_async_by_key = function(key, register)
  local cmd = ctx.registered_commands[key]
  assert(cmd, "command not found by key '" .. key .. "'")

  cmd:exec_register_async(register)
end

M.exec_range_async_by_key = function(key, from, to)
  local cmd = ctx.registered_commands[key]
  assert(cmd, "command not found by key '" .. key .. "'")

  cmd:exec_range_async(from, to)
end

M.setup = function(opts)
  assert(not ctx.setup_called, 'reg-exec.setup() called more than once')
  ctx.setup_called = true

  require'reg-exec.cmd'

  for k, v in pairs(opts.commands or {}) do
    k = tostring(k)

    ctx.registered_commands[k] = RegExecCommand:new(k, v)
  end

  ctx.telescope_opts = opts.telescope_opts

  opts.setup_default_range_command = opts.setup_default_range_command == nil or opts.setup_default_range_command
  if opts.setup_default_range_command then
    vim.cmd([[:command! -range RegExec lua require'reg-exec'.exec_range_async_by_telescope(<line1>, <line2>)]])
  end
end

return M
